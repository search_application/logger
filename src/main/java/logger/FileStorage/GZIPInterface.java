package logger.FileStorage;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Created by rahul on 29/12/15.
 */
public class GZIPInterface {

    public static byte[] decompressFile(File zipFile, String compressedFile) {

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        try {
            ZipFile zip = new ZipFile(zipFile);
            ZipEntry compresedEntry = zip.getEntry(compressedFile);
            InputStream is = zip.getInputStream(compresedEntry);

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return buffer.toByteArray();
    }

    public static void compressFile(MultipartFile file, String filename, File zipFile) {

        byte[] buffer = new byte[1024];


        try {

            FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            ZipEntry ze = new ZipEntry(filename);
            zos.putNextEntry(ze);


            InputStream in = file.getInputStream();

            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }

            in.close();
            zos.closeEntry();
            zos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
