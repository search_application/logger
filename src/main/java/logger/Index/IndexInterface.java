package logger.Index;

import logger.DataWrappers.MessageDataWrapper;
import logger.DataWrappers.MetadataWrapper;
import org.springframework.http.ResponseEntity;


/**
 * Created by rahul on 16/12/15.
 */

public interface IndexInterface {

    public ResponseEntity<String> indexMessage(MessageDataWrapper messageDataWrapper);

    public ResponseEntity<String> indexFile(MetadataWrapper metadataWrapper);
}
