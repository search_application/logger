package logger.Converter;

import logger.DataWrappers.FileMetadataWrapper;
import logger.DataWrappers.MetadataWrapper;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rahul on 19/12/15.
 *
 * An implementation of the FileConverter interface for .txt files.
 */
public class TextFileConverter implements FileConverter {

    /**
     * Extracts the text from .txt files as a string and returns a FileMetadataWrapper.
     *
     * @param stream        The InputStream of the MultiPartFile received from the request
     * @param filename      The name of the file
     * @param mimeType      The MIME type of the file
     * @param tags          The list of tags the file has been associated with
     * @param from          Name of the sender of the file
     * @param to            Name of the recipient of the file
     * @return              A FileMetadataWrapper Containing the whole text inside the document as a string.
     */
    @Override
    public List<MetadataWrapper> extractContent(InputStream stream, String filename, String mimeType, List<String> tags, String from, String to, boolean recursive) {

        FileMetadataWrapper wrapper = null;
        try {

            String content = String.valueOf(stream.read(new byte[stream.available()]));
            wrapper = new FileMetadataWrapper(filename, mimeType, content, tags, from, to);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(wrapper);

    }
}
