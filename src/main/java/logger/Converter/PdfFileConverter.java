package logger.Converter;

import logger.DataWrappers.FileMetadataWrapper;
import logger.DataWrappers.MetadataWrapper;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rahul on 17/12/15.
 *
 * An implementation of the FileConverter interface for PDF files. Uses the PF+DFBox library to extract the text from
 * PDF files as a string.
 */
public class PdfFileConverter implements FileConverter {

    /**
     * Extracts the text from the PDF file as a string and returns a FileMetadataWrapper
     *
     * @param stream        The InputStream of the MultiPartFile received from the request
     * @param filename      The name of the file
     * @param mimeType      The MIME type of the file
     * @param tags          The list of tags the file has been associated with
     * @param from          Name of the sender of the file
     * @param to            Name of the recipient of the file
     * @return              A FileMetadataWrapper Containing the whole text inside the document as a string.
     */
    @Override
    public List<MetadataWrapper> extractContent(InputStream stream, String filename, String mimeType, List<String> tags, String from, String to, boolean recursive) {

        String content = null;

        try {

            PDDocument pdfDocument = PDDocument.load(stream);
            PDFTextStripper pdfTextStripper = new PDFTextStripper();
            content = pdfTextStripper.getText(pdfDocument);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(new FileMetadataWrapper(filename, mimeType, content, tags, from, to));
    }
}
