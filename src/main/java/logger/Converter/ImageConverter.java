package logger.Converter;

import logger.DataWrappers.ImageMetadataWrapper;
import logger.DataWrappers.MetadataWrapper;
import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.DocumentBuilderFactory;
import org.apache.lucene.document.Document;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rahul on 18/01/16.
 *
 * An implementation of the FileConverter interface used to extract features from images. This converter extracts the
 * CEED feature from images and returns an ImageMetadataWrapper.
 */
public class ImageConverter implements FileConverter {

    /**
     * Parses the MultiPartFile object file as an image and extracts its CEDD feature as a string. The method then
     * wraps all the necessary in a ImageMetadataWrapper object and return it.
     *
     * @param stream        The InputStream of the MultiPartFile received from the request
     * @param filename      The name of the file
     * @param mimeType      The MIME type of the file
     * @param tags          The list of tags the file has been associated with
     * @param from          Name of the sender of the file
     * @param to            Name of the recipient of the file
     * @return              An ImageMetadataWrapper containing CEDD feature.
     */
    @Override
    public List<MetadataWrapper> extractContent(InputStream stream, String filename, String mimeType, List<String> tags, String from, String to, boolean recursive) {

        try {

            // Creating a CEDD document builder and indexing all files.
            DocumentBuilder builder = DocumentBuilderFactory.getCEDDDocumentBuilder();
            BufferedImage img = ImageIO.read(stream);

            Document document = builder.createDocument(img, filename);
            String CEDD = document.getField("featureCEDD").binaryValue().toString();

            return Arrays.asList(new ImageMetadataWrapper(filename, mimeType, CEDD, tags, from, to));

        } catch (IOException e) {
            e.printStackTrace();
            //TODO do some handling here
        }

        return null;
    }
}
